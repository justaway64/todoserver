var express = require("express");
var router = express.Router();
var models = require("../models");
const app = express();
const cors = require("cors");

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());
/* GET data */
router.get("/asd", async (req, res) => {
  try {
    const data = await models.Todogen2.findAll();
    console.log(data, "<< data backend masuk");
    res.status(200).json("asdasd");
  } catch (err) {
    console.log(err, "ini error backend");
  }
});

/* POST data */
router.post("/", function (req, res) {
  const { title, location, participant, date, note } = req.body;
  console.log(req.body);
  models.Todogen2.create({
    title,
    location,
    participant,
    date,
    note,
  }).then(function (hehe) {
    res.json(hehe);
  });
});

module.exports = router;
